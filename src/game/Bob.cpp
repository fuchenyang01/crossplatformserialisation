#include "Bob.h"





void Bob::set(float i, float j, float k, int x, int y, int z)
{
	this->i = i; 
	this->j = j;
	this->k = k;
	this->x = x;
	this->y = y;
	this->z = z;

}

float Bob::geti()
{
	return i;
}

float Bob::getj()
{
	return j;
}

float Bob::getk()
{
	return k;
}

int Bob::getx()
{
	return x;
}

int Bob::gety()
{
	return y;
}

int Bob::getz()
{
	return z;
}

void Bob::Write(OutputMemoryStream & inStream)
{
	inStream.Write(i);
	inStream.Write(j);
	inStream.Write(k);
	inStream.Write(x);
	inStream.Write(y);
	inStream.Write(z);
}

void Bob::Read(InputMemoryStream & inStream)
{
	inStream.Read(i);
	inStream.Read(j);
	inStream.Read(k);
	inStream.Read(x);
	inStream.Read(y);
	inStream.Read(z);
}
