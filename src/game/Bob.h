#pragma once


#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"



class Bob 
{
	float i,j, k;
	int x, y, z;
public:
	Bob():i(1), j(2), k(3), x(4.0f), y(5.0f), z(6.0f){}

	void set(float i, float j, float k, int x, int y, int z);
	float geti();
	float getj();
	float getk();
	int getx();
	int gety();
	int getz();

	void Write(OutputMemoryStream& inStream);

	void Read(InputMemoryStream& inStream);
};

