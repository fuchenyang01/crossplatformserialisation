#pragma once


#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include <string>


class Fred
{

	std::string name;
	std::vector<int> numbers;
	Vector3 position;
	Quaternion orientation;

public:
	Fred();
	Fred(std::string name, std::vector<int> numbers, Vector3 position, Quaternion orientation);



	void Write(OutputMemoryBitStream& out);

	void Read(InputMemoryBitStream& in);

};

