
#include "InputMemoryStream.h"


#include "OutputMemoryStream.h"


#if _WIN32
#include <Windows.h>
#endif

#include"Bob.h"
#include "Fred.h"

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

	
/*	std::shared_ptr<InputMemoryStream> in;

	std::shared_ptr<OutputMemoryStream>out;
	
	Bob bob; //client
	Bob bob2; //server

	bob.Write(*out);
	
	int copyLen = out->GetLength();
	char* copyBuff = new char[copyLen];
	
	
	//Imagin this is being written across the network!


	// Copy over the buffer
	memcpy(copyBuff, out->GetBufferPtr(), copyLen);
	
	// Create a new memory stream
	in.reset(new InputMemoryStream(copyBuff, copyLen));

	bob2.Read(*in);*/

	std::shared_ptr<InputMemoryBitStream> in;

	std::shared_ptr<OutputMemoryBitStream>out;

	Fred fred; //client
	Fred fred2; //server

	fred.Write(*out);

	int copyLen = out->GetBitLength()/8;
	copyLen++;
	char* copyBuff = new char[copyLen];

	memcpy(copyBuff, out->GetBufferPtr(), copyLen);

	in.reset(new InputMemoryStream(copyBuff, copyLen));

	fred2.Read(*in);


}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
